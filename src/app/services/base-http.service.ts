import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';


export class BaseService {

  extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  handleError(error: any) {
    return Observable.throw(error);
  }
}
