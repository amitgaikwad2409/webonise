import {Pipe, PipeTransform} from '@angular/core';
import {Student} from '../student-details/student';
import {SortEnum} from "./sort.enum";
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform{

    transform(array: Array<Student>, args: string): Array<Student> {
      if (typeof args === 'undefined') {
        return array;
      }
      return array.filter(student => (student.name.toLowerCase().indexOf(args) >= 0)
              || (student.rollNo.toString().indexOf(args) >= 0) );
    }
}
