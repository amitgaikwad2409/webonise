import {Pipe, PipeTransform} from '@angular/core';
import {Student} from '../student-details/student';
import {SortEnum} from "./sort.enum";
@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform{

    transform(array: Array<Student>, args: SortEnum): Array<Student> {
      if (typeof args === 'undefined') {
        return array;
      }
      if(!array){
        return array;
      }
      array.sort((a: Student, b: Student) => {
        switch (args) {
          case SortEnum.DATEOFBIRTH :
              if (a.dateOfBirth < b.dateOfBirth){
                return -1;
              }
              if (a.dateOfBirth > b.dateOfBirth){
                return 1;
              }
              return 0;
          case SortEnum.NAME:
            if (  a.name.localeCompare(b.name) === -1) {
              return -1;
            }
            if (a.name.localeCompare(b.name) === 1) {
              return 1;
            }
            return 0;
          case SortEnum.PERCENTAGE :
            if (a.percentage < b.percentage) {
              return 1;
            }
            if (a.percentage > b.percentage) {
              return -1;
            }
            return 0;
          case SortEnum.ROLL_NO :
            if (a.rollNo > b.rollNo) {
              return 1;
            }
            if (a.rollNo < b.rollNo) {
              return -1;
            }
            return 0;
        }
      });
      return array;
    }
}
