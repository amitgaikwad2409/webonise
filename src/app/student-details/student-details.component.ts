import { Component, OnInit } from '@angular/core';
import {StudentService} from './student.service';
import {Student} from './student';
import {SortEnum} from '../custom-pipes/sort.enum';
import {PagerService} from '../services/pager.service';
import {Pagination} from '../pagination/pagination-model';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  students: Student[]= new Array(0);
  sortOrder: SortEnum = SortEnum.PERCENTAGE;
  searchText: string;
  private paginationRecords = 10;
  pager: Pagination = new Pagination();
  data: any[];

  constructor(private studentService: StudentService,
              private pagerService: PagerService) { }

  ngOnInit() {
    this.studentService.get().subscribe(
      data => {
        this.data = data;
        this.setPage(1);
      },
      error => {
          console.error(error);
      });
  }

  getSortEnum() {
    return SortEnum;
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.data.length, page, this.paginationRecords);
    this.students = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

}
