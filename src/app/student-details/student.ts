export class Student {
  rollNo: number;
  name: string;
  percentage: number;
  dateOfBirth: Date;
}
