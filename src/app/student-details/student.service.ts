import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Student} from './student';
import {Http} from "@angular/http";
import {BaseService} from "../services/base-http.service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class StudentService extends BaseService {
  constructor(private http: Http) {
    super();
  }

  get(): Observable<Student[]> {
    let url: string = '/assets/students.json';
    return this.http.get(url).map(this.extractData);
  }

}
