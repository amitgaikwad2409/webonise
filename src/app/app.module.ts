import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import {StudentService} from './student-details/student.service';
import {HttpModule} from '@angular/http';
import {routing} from './routes';
import {SortPipe} from './custom-pipes/sort.pipe';
import {SearchPipe} from './custom-pipes/search.pipe';
import {FormsModule} from '@angular/forms';
import {PagerService} from './services/pager.service';

@NgModule({
  declarations: [
    AppComponent,
    SortPipe,
    StudentDetailsComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
  ],
  providers: [StudentService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
