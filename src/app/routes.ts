import {RouterModule} from '@angular/router';
import {StudentDetailsComponent} from "./student-details/student-details.component";


export const routing = RouterModule.forRoot([
  {
    path: '',
    pathMatch: 'full',
    component: StudentDetailsComponent
  },
  {
    path: 'student',
    pathMatch: 'full',
    component: StudentDetailsComponent
  }
]);
